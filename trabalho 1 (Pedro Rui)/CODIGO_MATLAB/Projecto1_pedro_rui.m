clear all;close all;clc;

pause(2)

global xobs_bk xobs_es xobs_tf Ko C_a phi_a gama_a;

%Parametros do motor dc fornecidos no enunciado
Ra=2.6;
Kma=0.0078; %%KE=KM
Ke=Kma;
Lm=0.18*10^(-3);
La=Lm;
Kgi=14;
Kge=5;
Jmrot=3.9*10^(-7);
Jm=4.60625*10^(-7);
Jl=6.63*10^(-5);
Jlext=5*10^(-5)
Jg=5.28*10^(-5);
Bm=0;
Bl=0;
Kf=Bm;

%% ************************************************************************
%Criterios de projeto:
disp('Criterios de dinamica de projecto');
tp1=0.2;
zeta=0.7
wn=pi/(tp1*sqrt(1-zeta^2))
h=0.01 %10 milisegundos

%% 1 ###################################################################### 
disp('Alinea 1 - Equa�oes pedidas');
Kg=Kgi*Kge;
Jeq=Kg^2*Jm+Jl%+Jlext
Beq=Kg^2*Bm+Bl
TL=0; %Binario na carga do motor
  
%% 2 ###################################################################### 
disp('Alinea 2 - Funcao de transferencia do motor Teta/Vin');
a0=0
a1=Kma*Kg
a2=(Ra*Jeq)/(Kma*Kg)
b0=1
num=[b0];
den=[a2 a1 a0];
trans_f=tf(num,den)
%% 3 ###################################################################### 
disp('Alinea 3 -Obtencao do espa�o de estados do servomotor');
trans_fz=tf(num,den,h)
% Obter A B C D respectiva � fun�ao de transferencia (Sistema continuo)
% A-> Matriz de dinamica
% B-> Vector de Comando
% C-> Saida
% D-> Matriz Nula
A=[0 1;-a0/a2 -a1/a2]
B=[0;b0/a2]
C=[1 0];
Ca=[1 0;0 1];
Da=[0;0];
C1=[1 0];
C2=[0 1];
D=0;

%% 4 ######################################################################
%% PD CONTINUO
disp('Alinea 4 - Projecto do Controlador PD');
Kd=((2*zeta*wn*a2)-a1)/b0
Kp=((wn*wn*a2)-a0)/b0
Ki=0;
% %**************************************************************************
% VARIAVEIS PARA SIMULINK:
% Bloco PID do simulink:

sr_step_time=1;
sr_initial_value=0;
sr_final_value=20;
sr_sample_time=0;

% PERTURBA�AO U
prt_u_step_time=3;
prt_u_initial_value=0;
prt_u_final_value=7;
prt_u_sample_time=0;

% PERTURBA�AO L
prt_l_step_time=0;
prt_l_initial_value=0;
prt_l_final_value=0;
prt_l_sample_time=0;

T=5;
Discreto=1; %1-> continuo 0-> discreto
slew_rate=50;
%Al_8_lc_la=1;

%INICIALIZACAO DAS MATRIZES DO OBSERVADOR:
xobs_bk=[0;0;0];
Ko=[2.6;182;140];
C_a=[C 0];
gama_a=[0.0039;0.7134;0];
phi_a=[1 0.007 0.0039; 0 0.61 0.71 ;0 0 1];

%La=[0.5292 -0.5215 1];
%Lc=0.5292;

xobs_es=xobs_bk
xobs_tf=xobs_bk


sim('simul_vf'); %chama simulink com PD
%Busca valores ao simulink
ref_time=ref.time;
ref_value=ref.signals.values;

teta_tf_time=teta_tf.time;
teta_tf_value=teta_tf.signals.values;

teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

teta_es_time=teta_es.time;
teta_es_value=teta_es.signals.values;

vel_ref_time=vel_ref.time;
vel_ref_value=vel_ref.signals.values;

vel_tf_time=vel_tf.time;
vel_tf_value=vel_tf.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

vel_es_time=vel_es.time;
vel_es_value=vel_es.signals.values;

figure('name',' ANGULO DE SAIDA CONTROLADOR PD - CONTROLADOR CONTINUO');
title('ANGULO DE SAIDA CONTROLADOR PD - CONTROLADOR CONTINUO');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_tf_time,teta_tf_value,'c',teta_bk_time,teta_bk_value,'-.r',teta_es_time,teta_es_value,'--k','LineWidth',2);
legend('Referencia','Teta tf','Teta bk','Teta es');

figure('name',' VELOCIDADE DE SAIDA CONTROLADOR PD - CONTROLADOR CONTINUO');
title('VELOCIDADE DE SAIDA CONTROLADOR PD - CONTROLADOR CONTINUO');
hold on;
grid on;
plot(vel_ref_time,vel_ref_value,'g',vel_tf_time,vel_tf_value,'c',vel_bk_time,vel_bk_value,'-.r',vel_es_time,vel_es_value,'--k','LineWidth',2);
legend('Referencia','Vel tf','Vel bk','Vel es');

%% 4 ######################################################################
%% PD DISCRETO
Discreto=0; %1-> continuo 0-> discreto

sim('simul_vf'); %chama simulink com PD
%Busca valores ao simulink
ref_time=ref.time;
ref_value=ref.signals.values;

teta_tf_time=teta_tf.time;
teta_tf_value=teta_tf.signals.values;

teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

teta_es_time=teta_es.time;
teta_es_value=teta_es.signals.values;

vel_ref_time=vel_ref.time;
vel_ref_value=vel_ref.signals.values;

vel_tf_time=vel_tf.time;
vel_tf_value=vel_tf.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

vel_es_time=vel_es.time;
vel_es_value=vel_es.signals.values;

figure('name',' ANGULO DE SAIDA CONTROLADOR PD - CONTROLADOR DISCRETO');
title('ANGULO DE SAIDA CONTROLADOR PD - CONTROLADOR DISCRETO');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_tf_time,teta_tf_value,'c',teta_bk_time,teta_bk_value,'-.r',teta_es_time,teta_es_value,'--k','LineWidth',2);
legend('Referencia','Teta tf','Teta bk','Teta es');

figure('name',' VELOCIDADE DE SAIDA CONTROLADOR PD - CONTROLADOR DISCRETO');
title('VELOCIDADE DE SAIDA CONTROLADOR PD - CONTROLADOR DISCRETO');
hold on;
grid on;
plot(vel_ref_time,vel_ref_value,'g',vel_tf_time,vel_tf_value,'c',vel_bk_time,vel_bk_value,'-.r',vel_es_time,vel_es_value,'--k','LineWidth',2);
legend('Referencia','Vel tf','Vel bk','Vel es');


%% 5 ######################################################################
%% PID CONTINUO
disp('Alinea 4 - Projecto do Controlador PID');
%% Parametros Controlador
p=8;
%formula canonica:
b0c=1/a2
a1c=a1/a2
a0c=a0/a2
a2c=1
Kp=(2*p*zeta*wn+wn^2-a0c)/b0c
Ki=(p*wn^2)/b0c
Kd=(2*zeta*wn+p-a1c)/b0c
% %**************************************************************************
% VARIAVEIS PARA SIMULINK:
% Bloco PID do simulink:
filter_bk=50;

sr_step_time=1;
sr_initial_value=0;
sr_final_value=20;
sr_sample_time=0;

% PERTURBA�AO U
prt_u_step_time=3;
prt_u_initial_value=0;
prt_u_final_value=7;
prt_u_sample_time=0;

% PERTURBA�AO L
prt_l_step_time=0;
prt_l_initial_value=0;
prt_l_final_value=0;
prt_l_sample_time=0;

T=5;
Discreto=1; %1-> continuo 0-> discreto

sim('simul_vf'); %chama simulink com PD
%Busca valores ao simulink
ref_time=ref.time;
ref_value=ref.signals.values;

teta_tf_time=teta_tf.time;
teta_tf_value=teta_tf.signals.values;

teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

teta_es_time=teta_es.time;
teta_es_value=teta_es.signals.values;

vel_ref_time=vel_ref.time;
vel_ref_value=vel_ref.signals.values;

vel_tf_time=vel_tf.time;
vel_tf_value=vel_tf.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

vel_es_time=vel_es.time;
vel_es_value=vel_es.signals.values;

figure('name',' ANGULO DE SAIDA CONTROLADOR PID - CONTROLADOR CONTINUO');
title('ANGULO DE SAIDA CONTROLADOR PID - CONTROLADOR CONTINUO');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_tf_time,teta_tf_value,'c',teta_bk_time,teta_bk_value,'-.r',teta_es_time,teta_es_value,'--k','LineWidth',2);
legend('Referencia','Teta tf','Teta bk','Teta es');

figure('name',' VELOCIDADE DE SAIDA CONTROLADOR PID - CONTROLADOR CONTINUO');
title('VELOCIDADE DE SAIDA CONTROLADOR PID - CONTROLADOR CONTINUO');
hold on;
grid on;
plot(vel_ref_time,vel_ref_value,'g',vel_tf_time,vel_tf_value,'c',vel_bk_time,vel_bk_value,'-.r',vel_es_time,vel_es_value,'--k','LineWidth',2);
legend('Referencia','Vel tf','Vel bk','Vel es');

%% 5 ######################################################################
%% PID DISCRETO
Discreto=0; %1-> continuo 0-> discreto

sim('simul_vf'); %chama simulink com PD
%Busca valores ao simulink
ref_time=ref.time;
ref_value=ref.signals.values;

teta_tf_time=teta_tf.time;
teta_tf_value=teta_tf.signals.values;

teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

teta_es_time=teta_es.time;
teta_es_value=teta_es.signals.values;

vel_ref_time=vel_ref.time;
vel_ref_value=vel_ref.signals.values;

vel_tf_time=vel_tf.time;
vel_tf_value=vel_tf.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

vel_es_time=vel_es.time;
vel_es_value=vel_es.signals.values;

figure('name',' ANGULO DE SAIDA CONTROLADOR PID - CONTROLADOR DISCRETO');
title('ANGULO DE SAIDA CONTROLADOR PID - CONTROLADOR DISCRETO');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_tf_time,teta_tf_value,'c',teta_bk_time,teta_bk_value,'-.r',teta_es_time,teta_es_value,'--k','LineWidth',2);
legend('Referencia','Teta tf','Teta bk','Teta es');

figure('name',' VELOCIDADE DE SAIDA CONTROLADOR PID - CONTROLADOR DISCRETO');
title('VELOCIDADE DE SAIDA CONTROLADOR PID - CONTROLADOR DISCRETO');
hold on;
grid on;
plot(vel_ref_time,vel_ref_value,'g',vel_tf_time,vel_tf_value,'c',vel_bk_time,vel_bk_value,'-.r',vel_es_time,vel_es_value,'--k','LineWidth',2);
legend('Referencia','Vel tf','Vel bk','Vel es');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 6 ######################################################################
% % DEADBEAT
% disp('Projecto de Observador de est�do aumentado, aplicar para estimar processo de perturba�ao a entrada');
% para eliminar o efeito da perturba�ao vamos calcular o modelo de espa�o de
% estados aumentado:
% xA'=phiA*xA+gamaA*U
% perturba��o constante( cw=1,pgiw=1)
% perturba��o na entrada(gama1=gama) 

%% PD CONTINUO
Kd=((2*zeta*wn*a2)-a1)/b0
Kp=((wn*wn*a2)-a0)/b0
Ki=0;
% %**************************************************************************
% VARIAVEIS PARA SIMULINK:
% Bloco PID do simulink:

sr_step_time=1;
sr_initial_value=0;
sr_final_value=12;
sr_sample_time=0;

% PERTURBA�AO U
prt_u_step_time=4;
prt_u_initial_value=0;
prt_u_final_value=4;
prt_u_sample_time=0;

% PERTURBA�AO L
prt_l_step_time=0;
prt_l_initial_value=0;
prt_l_final_value=0;
prt_l_sample_time=0;

T=10;
Discreto=0;
slew_rate=5;

[phi,gama]=c2d(A,B,h)
Cw=1;
phi_w=1
phi_a=[phi gama; 0 0 phi_w]
gama_a=[gama;0]
C_a=[C 0]
W0=[C_a;C_a*phi_a;C_a*(phi_a^2)]
Ko=phi_a^3*inv(W0)*[0;0;1]

xobs_bk=[0;0;0];
xobs_tf=[0;0;0];
xobs_es=[0;0;0];

sim('simul_vf'); %chama simulink com PD
%Busca valores ao simulink
ref_time=ref.time;
ref_value=ref.signals.values;

pert_time=pert.time;
pert_value=pert.signals.values;

pert_l_time=pert_l.time;
pert_l_value=pert_l.signals.values;

% para blocos do  motor
teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

x1_est_bk_time=x1_est_bk.time;
x1_est_bk_value=x1_est_bk.signals.values;

x2_est_bk_time=x2_est_bk.time;
x2_est_bk_value=x2_est_bk.signals.values;

p_est_bk_time=p_est_bk.time;
p_est_bk_value=p_est_bk.signals.values;
% para fun�ao de transferencia
teta_tf_time=teta_tf.time;
teta_tf_value=teta_tf.signals.values;

vel_tf_time=vel_tf.time;
vel_tf_value=vel_tf.signals.values;

x1_est_tf_time=x1_est_tf.time;
x1_est_tf_value=x1_est_tf.signals.values;

x2_est_tf_time=x2_est_tf.time;
x2_est_tf_value=x2_est_tf.signals.values;

p_est_tf_time=p_est_tf.time;
p_est_tf_value=p_est_tf.signals.values;

% para espa�o de estados
teta_es_time=teta_es.time;
teta_es_value=teta_es.signals.values;

vel_es_time=vel_es.time;
vel_es_value=vel_es.signals.values;

x1_est_es_time=x1_est_es.time;
x1_est_es_value=x1_est_es.signals.values;

x2_est_es_time=x2_est_es.time;
x2_est_es_value=x2_est_es.signals.values;

p_est_es_time=p_est_es.time;
p_est_es_value=p_est_es.signals.values;

% u_time=u.time;
% u_value=u.signals.values;

figure('name','SAIDAS DO OBSERVADOR -> POSIC�O - D. BLOCOS');
title('SINAL DE POSI�AO DO OBSERVADOR -> D. BLOCOS');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_bk_time,teta_bk_value,'r',x1_est_bk_time,x1_est_bk_value,'-.k','LineWidth',2);
legend('Referencia','Teta bk','x1 bk observador');

figure('name','SAIDAS DO OBSERVADOR -> VELOCIDADE - D. BLOCOS');
title('SINAL DE VELOCIDADE DO OBSERVADOR -> D. BLOCOS');
hold on;
grid on;
plot(vel_bk_time,vel_bk_value,'r',x2_est_bk_time,x2_est_bk_value,'-.k','LineWidth',2);
legend('velocidade bk','x2 bk observador');

figure('name','SAIDAS DO OBSERVADOR -> PERTURBACAO  - BLOCOS');
title('SINAL DE PERTURBACAO E DO OBSERVADOR -> BLOCOS');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_bk_time,p_est_bk_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao_bk observador');

%**************************************************************************
figure('name','SAIDAS DO OBSERVADOR -> POSIC�O - TF');
title('SINAL DE POSI�AO DO OBSERVADOR -> TF');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_tf_time,teta_tf_value,'r',x1_est_tf_time,x1_est_tf_value,'-.k','LineWidth',2);
legend('Referencia','Teta tf','x1 tf observador');

figure('name','SAIDAS DO OBSERVADOR -> VELOCIDADE - TF');
title('SINAL DE VELOCIDADE DO OBSERVADOR -> TF');
hold on;
grid on;
plot(vel_tf_time,vel_tf_value,'r',x2_est_tf_time,x2_est_tf_value,'-.k','LineWidth',2);
legend('velocidade tf','x2 tf observador');

figure('name','SAIDAS DO OBSERVADOR -> PERTURBACAO  - TF');
title('SINAL DE PERTURBACAO E DO OBSERVADOR -> TF');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_tf_time,p_est_tf_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao tf observador');

%**************************************************************************
figure('name','SAIDAS DO OBSERVADOR -> POSIC�O - ESPA�O ESTADOS');
title('SINAL DE POSI�AO DO OBSERVADOR ->  E. ESTADOS');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_es_time,teta_es_value,'r',x1_est_es_time,x1_est_es_value,'-.k','LineWidth',2);
legend('Referencia','Teta tf','x1 es observador');

figure('name','SAIDAS DO OBSERVADOR -> VELOCIDADE - ESPA�O ESTADOS');
title('SINAL DE VELOCIDADE DO OBSERVADOR ->  E. ESTADOS');
hold on;
grid on;
plot(vel_es_time,vel_es_value,'r',x2_est_es_time,x2_est_es_value,'-.k','LineWidth',2);
legend('velocidade tf','x2 es observador');

figure('name','SAIDAS DO OBSERVADOR -> PERTURBACAO  - ESPA�O ESTADOS');
title('SINAL DE PERTURBACAO E DO OBSERVADOR ->  E. ESTADOS');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_es_time,p_est_es_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao es observador');

% % 6 ######################################################################
% % % Observador com dinamica escolhida por n�s.
% %**************************************************************************
% VARIAVEIS PARA SIMULINK:
sr_step_time=1;
sr_initial_value=0;
sr_final_value=12;
sr_sample_time=0;

% PERTURBA�AO U
prt_u_step_time=4;
prt_u_initial_value=0;
prt_u_final_value=4;
prt_u_sample_time=0;

% PERTURBA�AO L
prt_l_step_time=0;
prt_l_initial_value=0;
prt_l_final_value=0;
prt_l_sample_time=0;

T=10;
Discreto=0;
%calculo do observador:
[phi,gama]=c2d(A,B,h)
p1=-0.2;
p2=-0.3;
p3=-0.4;
disp('Matriz de P_phi:')
P_phi= phi_a^3+p1*phi_a^2+p2*phi_a+p3*eye(3)
%calculo do novo k0
Ko=P_phi*inv(W0)*[0;0;1]

xobs_bk=[0;0;0];
xobs_tf=[0;0;0];
xobs_es=[0;0;0];

sim('simul_vf'); %chama simulink com PD
%Busca valores ao simulink
ref_time=ref.time;
ref_value=ref.signals.values;

pert_time=pert.time;
pert_value=pert.signals.values;

pert_l_time=pert_l.time;
pert_l_value=pert_l.signals.values;

% para blocos do  motor
teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

x1_est_bk_time=x1_est_bk.time;
x1_est_bk_value=x1_est_bk.signals.values;

x2_est_bk_time=x2_est_bk.time;
x2_est_bk_value=x2_est_bk.signals.values;

p_est_bk_time=p_est_bk.time;
p_est_bk_value=p_est_bk.signals.values;
% para fun�ao de transferencia
teta_tf_time=teta_tf.time;
teta_tf_value=teta_tf.signals.values;

vel_tf_time=vel_tf.time;
vel_tf_value=vel_tf.signals.values;

x1_est_tf_time=x1_est_tf.time;
x1_est_tf_value=x1_est_tf.signals.values;

x2_est_tf_time=x2_est_tf.time;
x2_est_tf_value=x2_est_tf.signals.values;

p_est_tf_time=p_est_tf.time;
p_est_tf_value=p_est_tf.signals.values;

% para espa�o de estados
teta_es_time=teta_es.time;
teta_es_value=teta_es.signals.values;

vel_es_time=vel_es.time;
vel_es_value=vel_es.signals.values;

x1_est_es_time=x1_est_es.time;
x1_est_es_value=x1_est_es.signals.values;

x2_est_es_time=x2_est_es.time;
x2_est_es_value=x2_est_es.signals.values;

p_est_es_time=p_est_es.time;
p_est_es_value=p_est_es.signals.values;


figure('name','SAIDAS DO OBSERVADOR -> POSIC�O - D. BLOCOS');
title('SINAL DE POSI�AO DO OBSERVADOR DIN. ESCOLH. -> D. BLOCOS');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_bk_time,teta_bk_value,'r',x1_est_bk_time,x1_est_bk_value,'-.k','LineWidth',2);
legend('Referencia','Teta bk','x1 bk observador');

figure('name','SAIDAS DO OBSERVADOR -> VELOCIDADE - D. BLOCOS');
title('SINAL DE VELOCIDADE DO OBSERVADOR DIN. ESCOLH. -> D. BLOCOS');
hold on;
grid on;
plot(vel_bk_time,vel_bk_value,'r',x2_est_bk_time,x2_est_bk_value,'-.k','LineWidth',2);
legend('velocidade bk','x2 bk observador');

figure('name','SAIDAS DO OBSERVADOR -> PERTURBACAO  - BLOCOS');
title('SINAL DE PERTURBACAO E DO OBSERVADOR DIN. ESCOLH. -> BLOCOS');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_bk_time,p_est_bk_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao bk observador');

%**************************************************************************
figure('name','SAIDAS DO OBSERVADOR -> POSIC�O - TF');
title('SINAL DE POSI�AO DO OBSERVADOR DIN. ESCOLH. -> TF');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_tf_time,teta_tf_value,'r',x1_est_tf_time,x1_est_tf_value,'-.k','LineWidth',2);
legend('Referencia','Teta tf','x1 bk observador');

figure('name','SAIDAS DO OBSERVADOR -> VELOCIDADE - TF');
title('SINAL DE VELOCIDADE DO OBSERVADOR DIN. ESCOLH. -> TF');
hold on;
grid on;
plot(vel_tf_time,vel_tf_value,'r',x2_est_tf_time,x2_est_tf_value,'-.k','LineWidth',2);
legend('velocidade tf','x2 bk observador');

figure('name','SAIDAS DO OBSERVADOR -> PERTURBACAO  - TF');
title('SINAL DE PERTURBACAO E DO OBSERVADOR DIN. ESCOLH. -> TF');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_tf_time,p_est_tf_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao tf observador');

%**************************************************************************
figure('name','SAIDAS DO OBSERVADOR -> POSIC�O - ESPA�O ESTADOS');
title('SINAL DE POSI�AO DO OBSERVADOR DIN. ESCOLH. ->  E. ESTADOS');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_es_time,teta_es_value,'r',x1_est_es_time,x1_est_es_value,'-.k','LineWidth',2);
legend('Referencia','Teta tf','x1 es observador');

figure('name','SAIDAS DO OBSERVADOR -> VELOCIDADE - ESPA�O ESTADOS');
title('SINAL DE VELOCIDADE DO OBSERVADOR DIN. ESCOLH. ->  E. ESTADOS');
hold on;
grid on;
plot(vel_es_time,vel_es_value,'r',x2_est_es_time,x2_est_es_value,'-.k','LineWidth',2);
legend('velocidade tf','x2 es observador');

figure('name','SAIDAS DO OBSERVADOR -> PERTURBACAO  - ESPA�O ESTADOS');
title('SINAL DE PERTURBACAO E DO OBSERVADOR DIN. ESCOLH.->  E. ESTADOS');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_es_time,p_est_es_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao es observador');

% ************************************************************************

%% ########################################################################
%% 7.1 -> adi�ao de binario na carga: -> mantendo a dinamica deadbeat
%% PD CONTINUO
Kd=((2*zeta*wn*a2)-a1)/b0
Kp=((wn*wn*a2)-a0)/b0
Ki=0;
% %***********************************************************************
% VARIAVEIS PARA SIMULINK:
% Bloco PID do simulink:
sr_step_time=1;
sr_initial_value=0;
sr_final_value=12;
sr_sample_time=0;

% PERTURBA�AO U
prt_u_step_time=0;
prt_u_initial_value=0;
prt_u_final_value=0;
prt_u_sample_time=0;

% PERTURBA�AO L
prt_l_step_time=4;
prt_l_initial_value=0;
prt_l_final_value=0.05;
prt_l_sample_time=0;

T=10;
Discreto=0;

%observador deadbeat
Ko=phi_a^3*inv(W0)*[0;0;1];

xobs_bk=[0;0;0];
xobs_tf=[0;0;0];
xobs_es=[0;0;0];

sim('simul_vf'); %chama simulink com PD
%Busca valores ao simulink
ref_time=ref.time;
ref_value=ref.signals.values;

pert_time=pert.time;
pert_value=pert.signals.values;

pert_l_time=pert_l.time;
pert_l_value=pert_l.signals.values;

% para blocos do  motor
teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

x1_est_bk_time=x1_est_bk.time;
x1_est_bk_value=x1_est_bk.signals.values;

x2_est_bk_time=x2_est_bk.time;
x2_est_bk_value=x2_est_bk.signals.values;

p_est_bk_time=p_est_bk.time;
p_est_bk_value=p_est_bk.signals.values;


figure('name','SAIDAS DO OBSERVADOR  PD (pert L.) -> POSIC�O - D. BLOCOS');
title('SINAL DE POSI�AO DO OBSERVADOR PD (pert L.) -> D. BLOCOS');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_bk_time,teta_bk_value,'r',x1_est_bk_time,x1_est_bk_value,'-.k','LineWidth',2);
legend('Referencia','Teta bk','x1 bk observador');

figure('name','SAIDAS DO OBSERVADOR  PD (pert L.) -> VELOCIDADE - D. BLOCOS');
title('SINAL DE VELOCIDADE DO OBSERVADOR  PD (pert L.) -> D. BLOCOS');
hold on;
grid on;
plot(vel_bk_time,vel_bk_value,'r',x2_est_bk_time,x2_est_bk_value,'-.k','LineWidth',2);
legend('velocidade bk','x2 bk observador');

figure('name','SAIDAS DO OBSERVADOR  PD (pert L.) -> PERTURBACAO  - BLOCOS');
title('SINAL DE PERTURBACAO E DO OBSERVADOR  PD (pert L.) -> BLOCOS');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_bk_time,p_est_bk_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao bk observador');

%% ########################################################################
%% PID CONTINUO
p=8;
%formula canonica:
b0c=1/a2
a1c=a1/a2
a0c=a0/a2
a2c=1
Kp=(2*p*zeta*wn+wn^2-a0c)/b0c
Ki=(p*wn^2)/b0c
Kd=(2*zeta*wn+p-a1c)/b0c

%observador deadbeat
Ko=phi_a^3*inv(W0)*[0;0;1];

xobs_bk=[0;0;0];
xobs_tf=[0;0;0];
xobs_es=[0;0;0];

sim('simul_vf'); %chama simulink com PD
%Busca valores ao simulink
ref_time=ref.time;
ref_value=ref.signals.values;

pert_time=pert.time;
pert_value=pert.signals.values;

pert_l_time=pert_l.time;
pert_l_value=pert_l.signals.values;

% para blocos do  motor
teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

x1_est_bk_time=x1_est_bk.time;
x1_est_bk_value=x1_est_bk.signals.values;

x2_est_bk_time=x2_est_bk.time;
x2_est_bk_value=x2_est_bk.signals.values;

p_est_bk_time=p_est_bk.time;
p_est_bk_value=p_est_bk.signals.values;


figure('name','SAIDAS DO OBSERVADOR  PID (pert L.) -> POSIC�O - D. BLOCOS');
title('SINAL DE POSI�AO DO OBSERVADOR  PID (pert L.) -> D. BLOCOS');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_bk_time,teta_bk_value,'r',x1_est_bk_time,x1_est_bk_value,'-.k','LineWidth',2);
legend('Referencia','Teta bk','x1 bk observador');

figure('name','SAIDAS DO OBSERVADOR  PID (pert L.) -> VELOCIDADE - D. BLOCOS');
title('SINAL DE VELOCIDADE DO OBSERVADOR  PID (pert L.) -> D. BLOCOS');
hold on;
grid on;
plot(vel_bk_time,vel_bk_value,'r',x2_est_bk_time,x2_est_bk_value,'-.k','LineWidth',2);
legend('velocidade bk','x2 bk observador');

figure('name','SAIDAS DO OBSERVADOR  PID (pert L.) -> PERTURBACAO  - BLOCOS');
title('SINAL DE PERTURBACAO E DO OBSERVADOR  PID (pert L.) -> BLOCOS');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_bk_time,p_est_bk_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao bk observador');

%% 7.2 -> altera��o do momento de inercia da carga:
%% PD CONTINUO
Kd=((2*zeta*wn*a2)-a1)/b0
Kp=((wn*wn*a2)-a0)/b0
Ki=0;
% %**************************************************************************
% VARIAVEIS PARA SIMULINK:
% Bloco PID do simulink:
sr_step_time=1;
sr_initial_value=0;
sr_final_value=12;
sr_sample_time=0;

% PERTURBA�AO U
prt_u_step_time=0;
prt_u_initial_value=0;
prt_u_final_value=0;
prt_u_sample_time=0;

% PERTURBA�AO L
prt_l_step_time=4;
prt_l_initial_value=0;
prt_l_final_value=0.05;
prt_l_sample_time=0;

%altera��o de valores para momento de inercia:
disp('Novo calculo do JL:')
rb=0.05*3
mb=0.04*10
Jg=5.28*10^-5
Jl=Jg + (mb*(rb)^2)/2 % momento de incercia do eixo da caixa de engrenagem

T=10;
Discreto=0;

%observador deadbeat
Ko=phi_a^3*inv(W0)*[0;0;1];

xobs_bk=[0;0;0];
xobs_tf=[0;0;0];
xobs_es=[0;0;0];

sim('simul_vf'); %chama simulink com PD
%Busca valores ao simulink
ref_time=ref.time;
ref_value=ref.signals.values;

pert_time=pert.time;
pert_value=pert.signals.values;

pert_l_time=pert_l.time;
pert_l_value=pert_l.signals.values;

% para blocos do  motor
teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

x1_est_bk_time=x1_est_bk.time;
x1_est_bk_value=x1_est_bk.signals.values;

x2_est_bk_time=x2_est_bk.time;
x2_est_bk_value=x2_est_bk.signals.values;

p_est_bk_time=p_est_bk.time;
p_est_bk_value=p_est_bk.signals.values;


figure('name','SAIDAS DO OBSERVADOR  PD (pert L.) -> POSIC�O - D. BLOCOS');
title('SINAL DE POSI�AO DO OBSERVADOR PD (pert L.) -> D. BLOCOS');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_bk_time,teta_bk_value,'r',x1_est_bk_time,x1_est_bk_value,'-.k','LineWidth',2);
legend('Referencia','Teta bk','x1 bk observador');

figure('name','SAIDAS DO OBSERVADOR  PD (pert L.) -> VELOCIDADE - D. BLOCOS');
title('SINAL DE VELOCIDADE DO OBSERVADOR  PD (pert L.) -> D. BLOCOS');
hold on;
grid on;
plot(vel_bk_time,vel_bk_value,'r',x2_est_bk_time,x2_est_bk_value,'-.k','LineWidth',2);
legend('velocidade bk','x2 bk observador');

figure('name','SAIDAS DO OBSERVADOR  PD (pert L.) -> PERTURBACAO  - BLOCOS');
title('SINAL DE PERTURBACAO E DO OBSERVADOR  PD (pert L.) -> BLOCOS');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_bk_time,p_est_bk_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao bk observador');

%% ########################################################################
%% PID CONTINUO
p=8;
%formula canonica:
b0c=1/a2
a1c=a1/a2
a0c=a0/a2
a2c=1
Kp=(2*p*zeta*wn+wn^2-a0c)/b0c
Ki=(p*wn^2)/b0c
Kd=(2*zeta*wn+p-a1c)/b0c

xobs_bk=[0;0;0];
xobs_tf=[0;0;0];
xobs_es=[0;0;0];

sim('simul_vf'); %chama simulink com PD
%Busca valores ao simulink
ref_time=ref.time;
ref_value=ref.signals.values;

pert_time=pert.time;
pert_value=pert.signals.values;

pert_l_time=pert_l.time;
pert_l_value=pert_l.signals.values;

% para blocos do  motor
teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

x1_est_bk_time=x1_est_bk.time;
x1_est_bk_value=x1_est_bk.signals.values;

x2_est_bk_time=x2_est_bk.time;
x2_est_bk_value=x2_est_bk.signals.values;

p_est_bk_time=p_est_bk.time;
p_est_bk_value=p_est_bk.signals.values;


figure('name','SAIDAS DO OBSERVADOR  PID (pert L.) -> POSIC�O - D. BLOCOS');
title('SINAL DE POSI�AO DO OBSERVADOR  PID (pert L.) -> D. BLOCOS');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_bk_time,teta_bk_value,'r',x1_est_bk_time,x1_est_bk_value,'-.k','LineWidth',2);
legend('Referencia','Teta bk','x1 bk observador');

figure('name','SAIDAS DO OBSERVADOR  PID (pert L.) -> VELOCIDADE - D. BLOCOS');
title('SINAL DE VELOCIDADE DO OBSERVADOR  PID (pert L.) -> D. BLOCOS');
hold on;
grid on;
plot(vel_bk_time,vel_bk_value,'r',x2_est_bk_time,x2_est_bk_value,'-.k','LineWidth',2);
legend('velocidade bk','x2 bk observador');

figure('name','SAIDAS DO OBSERVADOR  PID (pert L.) -> PERTURBACAO  - BLOCOS');
title('SINAL DE PERTURBACAO E DO OBSERVADOR  PID (pert L.) -> BLOCOS');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_bk_time,p_est_bk_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao bk observador');
 
%% ########################################################################
%% 8 -> Aplica��o de realimenta��o no espa�o de estados:

% VARIAVEIS PARA SIMULINK:
% Bloco PID do simulink:
sr_step_time=1;
sr_initial_value=0;
sr_final_value=12;
sr_sample_time=0;

% PERTURBA�AO U
prt_u_step_time=4;
prt_u_initial_value=0;
prt_u_final_value=7;
prt_u_sample_time=0;

% PERTURBA�AO L
prt_l_step_time=6;
prt_l_initial_value=0;
prt_l_final_value=0.1;
prt_l_sample_time=0;


Jl=6.63*10^(-5);

 zeta
 wn
 den_e=[1 2*zeta*wn wn*wn];
 [a,b,c,d]=tf2ss([0 0 1], den_e)
 [phi_cl,gama_cl]=c2d(a,b,h)
 p_cl=eig(phi_cl)
 L=acker(phi,gama,p_cl)
 Lw=1;
 L_a=[L Lw]

 %Ganho Lc para seguimento com ganho DC unitario
 phic=phi-gama*L
 Lc=1/(C*inv(eye(2)-phic)*gama)
 
% %observador deadbeat
% Ko=phi_a^3*inv(W0)*[0;0;1];

xobs_bk=[0;0;0];
xobs_tf=[0;0;0];
xobs_es=[0;0;0];

sim('Simul_ali'); %chama simulink com PID


ref_time=ref.time;
ref_value=ref.signals.values;

pert_time=pert.time;
pert_value=pert.signals.values;

pert_l_time=pert_l.time;
pert_l_value=pert_l.signals.values;

% para blocos do  motor
teta_bk_time=teta_bk.time;
teta_bk_value=teta_bk.signals.values;

vel_bk_time=vel_bk.time;
vel_bk_value=vel_bk.signals.values;

x1_est_bk_time=x1_est_bk.time;
x1_est_bk_value=x1_est_bk.signals.values;

x2_est_bk_time=x2_est_bk.time;
x2_est_bk_value=x2_est_bk.signals.values;

p_est_bk_time=p_est_bk.time;
p_est_bk_value=p_est_bk.signals.values;


figure('name','SINAL DE POSI�AO ESTADO AUMENTADO REALIMENTADO  -D. BLOCOS');
title('SINAL DE POSI�AO ESTADO AUMENTADO REALIMENTADO  -D. BLOCOS');
hold on;
grid on;
plot(ref_time,ref_value,'g',teta_bk_time,teta_bk_value,'r',x1_est_bk_time,x1_est_bk_value,'-.k','LineWidth',2);
legend('Referencia','Teta bk','x1 bk observador');

figure('name','SINAL DE PERTURBACAO ESTADO AUMENTADO REALIMENTADO  -D. BLOCOS');
title('SINAL DE VELOCIDADE ESTADO AUMENTADO REALIMENTADO  -D. BLOCOS');
hold on;
grid on;
plot(vel_bk_time,vel_bk_value,'r',x2_est_bk_time,x2_est_bk_value,'-.k','LineWidth',2);
legend('velocidade bk','x2 bk observador');

figure('name','SAIDAS DE PERTURBACAO: ESTADO AUMENTADO REALIMENTADO  -D. BLOCOS');
title('SINAL DE PERTURBACAO ESTADO AUMENTADO REALIMENTADO  -D. BLOCOS');
hold on;
grid on;
plot(pert_time,pert_value,'r',pert_l_time,pert_l_value,'-.c',p_est_bk_time,p_est_bk_value,'k','LineWidth',2);
legend('Ref perturbacao U','Ref perturbacao L','perturbacao bk observador');






