function estim_1=obs_bk(ent)
    global xobs_bk Ko C_a phi_a gama_a;
    uk1_bk=ent(1);
    yok_bk=ent(2);
    %predi��o do estado aumentado
    xobs_bk=phi_a*xobs_bk+gama_a*uk1_bk+Ko*(yok_bk-C_a*xobs_bk);
    %Devolu��o do estado observado
    estim_1=xobs_bk;
end
