function estim_3=obs_tf(ent)
    global xobs_tf Ko C_a phi_a gama_a;
    uk1_tf=ent(1);
    yok_tf=ent(2);
    %predi��o do estado aumentado
    xobs_tf=phi_a*xobs_tf+gama_a*uk1_tf+Ko*(yok_tf-C_a*xobs_tf);
    %Devolu��o do estado observado
    estim_3=xobs_tf;

