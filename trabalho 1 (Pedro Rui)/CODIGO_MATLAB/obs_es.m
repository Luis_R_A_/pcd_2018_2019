function estim_2=obs_es(ent)
    global xobs_es Ko C_a phi_a gama_a;
    uk1_es=ent(1);
    yok_es=ent(2);
    %predi��o do estado aumentado
    xobs_es=phi_a*xobs_es+gama_a*uk1_es+Ko*(yok_es-C_a*xobs_es);
    %Devolu��o do estado observado
    estim_2=xobs_es;
end
