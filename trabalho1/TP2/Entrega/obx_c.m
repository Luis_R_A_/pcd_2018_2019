function estim=obx_c(ent)
%global phi_a xobs Ko C gama;
global phi_a xobs_a Ko_a C_a gama_a;

uk1=ent(1);
yok=ent(2);
%predi��o do estado aumentado
xobs_a=phi_a*xobs_a+gama_a*uk1 + Ko_a*(yok-C_a*xobs_a);
%Devolu��o do estado observado
estim=xobs_a;

