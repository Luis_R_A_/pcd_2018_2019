clear, clc, close all

global phi_a xobs_a Ko_a C_a gama_a xobs_a_2;

%% Disturbance selection
signal_select = 0; %0 for step. 1 for signal generator.
D_Amp = 0.2;


%% System parameters
A=[0 1; 0 -3]; B=[0;2]; C=[1 0]; D=0;
h = 0.25;

%% Augmented system parameters and "filters"
C1=[1 0];
C2=[0 1]; 
Ca=[1 0;0 1];
Da=[0;0];

%% Convert system to discrete
[phi,gama]=c2d(A,B,h) % modelo de estado do sistema discreto
%[num, den]=ss2tf(phi,gama,C,D,1)
%sysd=tf(num,den,h) %função de transferência do sist. discreto

%% Get controller desired poles 
zeta=0.8, wn=1;
den=[1 2*zeta*wn wn*wn];
[a,b,c,d]=tf2ss([0 0 1], den)
[phi_cl,gama_cl]=c2d(a,b,h)
p_cl= eig(phi_cl)

%% Calculated desired L for normal feedback
L= acker(phi, gama, p_cl) 
Lobs = L;
%% Get controler Lc
phic=phi-gama*L;
Lc=1/(C*inv(eye(2)-phic)*gama);

%% Initial condition
x0=0;




%% augmented observer


Lw = [1, 0];
La_obs = [Lobs, Lw];
xobs_a = [0;0;0;0];
w0 = 1;
Aw = [0, 1; -(w0^2) 0];
Bw = [0; w0];
Cw = [1 0];
Dw = [0];
[phiw,gamaw]=c2d(Aw,Bw,h);
phi_a=[phi gama*Cw; [0 0;0 0], phiw];
gama_a=[gama;0;0];
C_a=[C 0,0];
w0=[C_a; C_a*phi_a; C_a*phi_a^2; C_a*phi_a^3];
alpha1 = -0.2;
alpha2 = 0;
alpha3 = 0;
alpha4 = 0;
Pphi = phi_a^4 + alpha1*phi_a^3 + alpha2*phi_a^2 + alpha3*phi_a^1 + alpha4*eye(4);
Ko_a=Pphi*inv(w0)*[0; 0; 0; 1];
%Ko_a=phi_a^4*inv(w0)*[0; 0; 0; 1];

xobs_a_2 = xobs_a;
%% simulate
sim('Part2_simulink.slx')


%% show plots
figure;
subplot(1,3,1)
plot(u_1.time, u_1.signals.values,'r-', x1_1.time, x1_1.signals.values,'b--',...
x2_1.time, x2_1.signals.values,'k:','LineWidth',2)
title('Malha aberta - perturbação em degrau')
legend('u', 'x1','x2')
grid 

subplot(1,3,2)
plot(u_1.time, u_1.signals.values,'r-', x1_obs1.time, x1_obs1.signals.values,'b--',...
 x2_obs1.time, x2_obs1.signals.values,'k:' ,'LineWidth',2)
title('Malha aberta - perturbação em degrau')
legend('u', 'x1 obs', 'x2 obs')
grid 

subplot(1,3,3)
plot(p.time, p.signals.values,'r-', p1_obs1.time, p1_obs1.signals.values,'b--',...
p2_obs1.time, p2_obs1.signals.values,'k:', 'LineWidth',2)
title('Malha aberta - perturbação em degrau')
legend('p', 'p1 obs', 'p2 obs')
grid 

figure;
subplot(1,3,1)
plot(u.time, u.signals.values,'r-', y.time, y.signals.values,'b--',...
'LineWidth',2)
title('Malha fechada - perturbação em degrau')
legend('u', 'x1','x2')
grid 

subplot(1,3,2)
plot(u.time, u.signals.values,'r-', x1_obs.time, x1_obs.signals.values,'b--',...
x2_obs.time, x2_obs.signals.values,'k:' ,'LineWidth',2)
title('Malha fechada - perturbação em degrau')
legend('u', 'x1 obs', 'x2 obs')
grid 

subplot(1,3,3)
plot(p.time, p.signals.values,'r-', p1_obs.time, p1_obs.signals.values,'b--',...
p2_obs.time, p2_obs.signals.values,'k:', 'LineWidth',2)
title('Malha fechada - perturbação em degrau')
legend('p', 'p1 obs', 'p2 obs')
grid 
%% sinusoidal
signal_select = 1; %0 for step. 1 for signal generator.
xobs_a = [0;0;0;0];
xobs_a_2 = xobs_a;
%% simulate
sim('Part2_simulink.slx')

%% show plots
figure;
subplot(1,3,1)
plot(u_1.time, u_1.signals.values,'r-', x1_1.time, x1_1.signals.values,'b--',...
x2_1.time, x2_1.signals.values,'k:','LineWidth',2)
title('Malha aberta - perturbação sinusoidal')
legend('u', 'x1','x2')
grid 

subplot(1,3,2)
plot(u_1.time, u_1.signals.values,'r-', x1_obs1.time, x1_obs1.signals.values,'b--',...
 x2_obs1.time, x2_obs1.signals.values,'k:' ,'LineWidth',2)
title('Malha aberta - perturbação sinusoidal')
legend('u', 'x1 obs1', 'x2 obs1')
grid 

subplot(1,3,3)
plot(p.time, p.signals.values,'r-', p1_obs1.time, p1_obs1.signals.values,'b--',...
p2_obs1.time, p2_obs1.signals.values,'k:', 'LineWidth',2)
title('Malha aberta - perturbação sinusoidal')
legend('p', 'p1 obs1', 'p2 obs1')
grid 

figure;
subplot(1,3,1)
plot(u.time, u.signals.values,'r-', y.time, y.signals.values,'b--',...
'LineWidth',2)
title('Malha fechada - perturbação sinusoidal')
legend('u', 'x1','x2')
grid 

subplot(1,3,2)
plot(u.time, u.signals.values,'r-', x1_obs.time, x1_obs.signals.values,'b--',...
x2_obs.time, x2_obs.signals.values,'k:' ,'LineWidth',2)
title('Malha fechada - perturbação sinusoidal')
legend('u', 'x1 obs', 'x2 obs')
grid 

subplot(1,3,3)
plot(p.time, p.signals.values,'r-', p1_obs.time, p1_obs.signals.values,'b--',...
p2_obs.time, p2_obs.signals.values,'k:', 'LineWidth',2)
title('Malha fechada - perturbação sinusoidal')
legend('p', 'p1 obs', 'p2 obs')
grid 