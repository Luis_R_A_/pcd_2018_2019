clear; close all; clc;

%% 
% Vari�veis do sistema
global G_num G_den tau h

% Ganhos de controlador
global Kp Ki Kd

% Vari�veis de simula��o
global R_amp R_time D_amp D_time

% Vari�veis de Enunciado
G_num = [2];
G_den = [1 3 0];
tau = 0.6;
h = 0.25;
zeta = 0.8;
wn = 0.8;

% Vari�veis de Simula��o
R_amp = 1;
R_time = 10;
D_amp = 0.2;
D_time = 20;

%% Controlador
% parte real dos p�los conjugados
Re = -zeta*wn;
p = -10*Re;

%          2
% G = ----------- * exp(-0.6)
%      s^2 + 3 s
b0 = 2;
a0 = 0;
a1 = 3;

Kp = (2*zeta*wn*p + wn^2 - a0)/b0;
Ki = (p*wn^2)/b0;
Kd = (p + 2*zeta*wn - a1)/b0;


sim('Part3_simulink.slx');

subplot(1,3,1);
plot(U2.time, U2.signals.values,'r-', Y2.time, Y2.signals.values,'b--',...
 R.time, R.signals.values,'k:' ,'LineWidth',2)
title('PID - Sem atraso')
legend('u', 'y', 'ref')
grid 

subplot(1,3,2);
plot(U1.time, U1.signals.values,'r-', Y1.time, Y1.signals.values,'b--',...
 R.time, R.signals.values,'k:' ,'LineWidth',2)
title('PID - Atraso - Sem preditor')
axis([0 30 -30 30])
legend('u', 'y', 'ref')
grid 

subplot(1,3,3);
plot(U.time, U.signals.values,'r-', Y.time, Y.signals.values,'b--',...
 R.time, R.signals.values,'k:' ,'LineWidth',2)
title('PID - Atraso - Preditor Smith')
legend('u', 'y', 'ref')
grid 