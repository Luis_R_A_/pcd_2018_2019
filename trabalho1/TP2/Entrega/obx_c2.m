function estim=obx_c2(ent)
%global phi_a xobs Ko C gama;
global phi_a xobs_a_2 Ko_a C_a gama_a;

uk1=ent(1);
yok=ent(2);
%predi��o do estado aumentado
xobs_a_2=phi_a*xobs_a_2+gama_a*uk1 + Ko_a*(yok-C_a*xobs_a_2);
%Devolu��o do estado observado
estim=xobs_a_2;

