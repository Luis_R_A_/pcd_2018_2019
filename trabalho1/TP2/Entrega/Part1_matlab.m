clear, clc, close all

%% Disturbance selection
signal_select = 0; %0 for step. 1 for signal generator.
D_Amp = 0.2;


%% System parameters
A=[0 1; 0 -3]; B=[0;2]; C=[1 0]; D=0;
h = 0.25;

%% Augmented system parameters and "filters"
C1=[1 0];
C2=[0 1]; 
Ca=[1 0;0 1];
Da=[0;0];

%% Convert system to discrete
[phi,gama]=c2d(A,B,h) % modelo de estado do sistema discreto
%[num, den]=ss2tf(phi,gama,C,D,1)
%sysd=tf(num,den,h) %fun��o de transfer�ncia do sist. discreto

%% Get controller desired poles 
zeta=0.8, wn=1;
den=[1 2*zeta*wn wn*wn];
[a,b,c,d]=tf2ss([0 0 1], den)
[phi_cl,gama_cl]=c2d(a,b,h)
p_cl=eig(phi_cl)

%% Calculated desired L for normal feedback
L=acker(phi, gama, p_cl) 
L2 = L;
%% Get controler Lc
phic=phi-gama*L;
Lc=1/(C*inv(eye(2)-phic)*gama);

%% Initial condition
x0=[0.1;0];
L1 = 0;
%% simulate
sim('Part1_simulink.slx')

%% show plots
figure;
subplot(1,3,1)
plot(u_1.time, u_1.signals.values,'r-', x1_1.time, x1_1.signals.values,'b--',...
x2_1.time, x2_1.signals.values,'k:','LineWidth',2)
title('Controlador sem ac��o integral - perturba��o em degrau')
legend('u', 'x1','x2')
grid 

%% Integral controller La, L, L, LC. ======================================
phi_a= [1 -C; [0;0] phi];
gama_a=[0;gama];
newPole = 0.3;
p_a = [p_cl; newPole];


La = acker(phi_a, gama_a, p_a);
L1 = -La(1);
L = La(2:3);
Lc = L1/(1-newPole);

%% simulate
sim('Part1_simulink.slx')

%% show plots
subplot(1,3,2)
plot(u_1.time, u_1.signals.values,'r-', x1_1.time, x1_1.signals.values,'b--',...
x2_1.time, x2_1.signals.values,'k:','LineWidth',2)
title('Controlador com ac��o integral - perturba��o em degrau')
legend('u', 'x1','x2')
grid 

%% Sinusoidal disturbance =================================================
signal_select = 1; %0 for step. 1 for signal generator.
%% simulate
sim('Part1_simulink.slx')

%% show plots
subplot(1,3,3)
plot(u_1.time, u_1.signals.values,'r-', x1_1.time, x1_1.signals.values,'b--',...
x2_1.time, x2_1.signals.values,'k:','LineWidth',2)
title('Controlador com ac��o integral - perturba��o sinusoidal')
legend('u', 'x1','x2')
grid 