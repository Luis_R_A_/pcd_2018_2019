function estim=obx3(ent)
%global phi_a xobs Ko C gama;
global phi_a2 xobs_a3 Ko_a2 C_a2 gama_a2;

uk1=ent(1);
yok=ent(2);
%predi��o do estado aumentado
xobs_a3=phi_a2*xobs_a3+gama_a2*uk1 + Ko_a2*(yok-C_a2*xobs_a3);
%Devolu��o do estado observado
estim=xobs_a3;

