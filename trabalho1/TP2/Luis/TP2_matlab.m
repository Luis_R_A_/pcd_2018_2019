clear, clc, close all
%global phi xobs Ko C gama;
global phi_a xobs_a Ko_a C_a gama_a xobs_a2;
global phi_a2 xobs_a3 Ko_a2 C_a2 gama_a2;
signal_select = 1; %0 for step. 1 for signal generator.
D_Amp = 0.2;


A=[0 1; 0 -3]; B=[0;2]; C=[1 0]; D=0;
h = 0.5;
C1=[1 0];
C2=[0 1]; 

Ca=[1 0;0 1];
Da=[0;0];

[phi,gama]=c2d(A,B,h) % modelo de estado do sistema discreto
[num, den]=ss2tf(phi,gama,C,D,1)
sysd=tf(num,den,h) %função de transferência do sist. discreto
%Controlador: definicao do polinomio caracteristico
zeta=0.8, wn=1;
den=[1 2*zeta*wn wn*wn];
[a,b,c,d]=tf2ss([0 0 1], den)
[phi_cl,gama_cl]=c2d(a,b,h)
p_cl=eig(phi_cl)
L=acker(phi, gama, p_cl)

Lobs = L;

L1 = 0;

phic=phi-gama*L;
Lc=1/(C*inv(eye(2)-phic)*gama) ;

x0=[0;0];


%% Parte diferente do integral
%phiw=1; phixw=gama;
%phi_a=[phi phixw; 0 0 phiw];
phi_a= [1 -C; [0;0] phi];
%gama_a=[gama;0];
gama_a=[0;gama];
newPole = 0.3;
p_a = [p_cl; newPole];


La = acker(phi_a, gama_a, p_a);
L1 = -La(1);
L = La(2:3);
Lc = L1/(1-newPole);


%% Observer related
xobs=[0;0]
w0=[C; C*phi]
Ko=phi^2*inv(w0)*[0; 1]

%% parte do aumentado
Lw=1;
La_obs=[Lobs Lw];
phiw=1; phixw=gama;
phi_a=[phi phixw; 0 0 phiw];
gama_a=[gama;0];
C_a=[C 0];
xobs_a=[0;0;0];
x0_a = 0;
w0=[C_a; C_a*phi_a; C_a*phi_a^2]
Ko_a=phi_a^3*inv(w0)*[0; 0; 1];

%% for second observer
xobs_a2 = xobs_a;


%% La for sinusoidal
Lw2 = [1, 0];
La_obs2 = [Lobs, Lw2];
xobs_a3 = [xobs_a; 0];
w0 = 1;
Aw = [0, 1; -(w0^2) 0];
Bw = [0; w0];
Cw = [1 0];
Dw = [0];
[phiw,gamaw]=c2d(Aw,Bw,h);
phi_a2=[phi gama*Cw; [0 0;0 0], phiw];
gama_a2=[gama;0;0];
C_a2=[C 0,0];
w0=[C_a2; C_a2*phi_a2; C_a2*phi_a2^2; C_a2*phi_a2^3]
Ko_a2=phi_a2^4*inv(w0)*[0; 0; 0; 1];
%% simulate
sim('TP2_simulink.slx')


%% show plots
figure;
plot(u_1.time, u_1.signals.values,'r-', x1_1.time, x1_1.signals.values,'b--',...
x2_1.time, x2_1.signals.values,'k:','LineWidth',2)
title('Realimentação de estados - Estado do sistema e sinal de comando com perturbaçao')
legend('u', 'x1','x2')
grid 